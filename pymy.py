#!/usr/bin/env python

import pymysql, re


def Get_DBQuery(input):
	#print(input)
	prefix = "SELECT samid, fn, mi, ln, display FROM `wiki_users`.`ad_disabled` WHERE `ln`=\'"


	if len(input) > 1:
		return prefix + input[0].strip() + "' AND `fn` = '" + input[1].strip() +"';"
	else:
		return prefix + input[0].strip() + "';"


def Get_LDAPQuery(input):
	'''
	Returns an LDAP query to fetch these attributes:
	sAMAccountName, givenName, initials, sn, displayName
	'''

def prompt():
	var = raw_input("Enter a name (last | last,first | [none]=quit)\n\t>: ")
	return var


def main():
	while True:
		print("\n\n")

		var = prompt()

		if len(var) == 0:
			break

		q = Get_DBQuery(re.findall(r"[^,][\w]+", var))
		cur.execute(q)

		print('')
		vals = cur.fetchall()
		for val in vals:
			val = [str(v).replace('None', ' ') for v in val]
			print '{0:15} {1:24} {2:4} {3:22} {4:40}'.format(*val)
			# print(val)

conn = pymysql.connect(host='dhsct1048258.hr.state.or.us', port=3306,
						user='db_user', passwd='KeenanDB', db='wiki_users')
cur = conn.cursor()


if __name__ == '__main__':	
	main()

cur.close()
conn.close()	
	

